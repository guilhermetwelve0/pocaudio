﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using PocAudio.Data;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using PocAudio.Models;
using Microsoft.Extensions.Hosting.Internal;

namespace PocAudio.Controllers
{
    public class AudioController : Controller
    {
        
        private readonly IWebHostEnvironment _hostingEnvironment;

        public AudioController(IWebHostEnvironment hostingEnvironment)
        {
            
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            // Exibir a página principal com um formulário para gravar áudio
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GravarAudio(IFormFile audio)
        {
            try
            {
                if (audio != null && audio.Length > 0)
                {
                    var dataFolder = Path.Combine(_hostingEnvironment.ContentRootPath, "Data");
                    if (!Directory.Exists(dataFolder))
                    {
                        Directory.CreateDirectory(dataFolder);
                    }
                    var filePath = Path.Combine(dataFolder, audio.FileName);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await audio.CopyToAsync(fileStream);
                    }

                    // Convertendo o arquivo para MP3
                    var mp3FilePath = Path.ChangeExtension(filePath, ".mp3");
                    var process = new Process
                    {
                        StartInfo = new ProcessStartInfo
                        {
                            FileName = "ffmpeg",
                            Arguments = $"-i {filePath} {mp3FilePath}",
                            RedirectStandardOutput = true,
                            RedirectStandardError = true,
                            UseShellExecute = false,
                            CreateNoWindow = true,
                        }
                    };
                    process.Start();
                    string output = process.StandardOutput.ReadToEnd();
                    string errors = process.StandardError.ReadToEnd();
                    process.WaitForExit();
                    Console.WriteLine(output);
                    Console.WriteLine(errors);

                    // Opcional: Exclua o arquivo original WebM após a conversão
                    System.IO.File.Delete(filePath);

                    var audioModel = new AudioModel
                    {
                        Nome = Path.GetFileName(mp3FilePath),
                        Caminho = mp3FilePath,
                        DataGravacao = DateTime.Now
                    };



                    return Json(new { sucesso = true, mensagem = "Áudio gravado com sucesso!" });
                }
                else
                {
                    return Json(new { sucesso = false, mensagem = $"Arquivo de áudio inválido. Length: {audio?.Length}" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { sucesso = false, mensagem = "Erro ao gravar o áudio: " + ex.Message });
            }
        }


        [HttpPost]
        public IActionResult ExcluirAudio()
        {
            try
            {
                // Lógica para excluir o áudio do banco de dados
               

                return Json(new { sucesso = true, mensagem = "Áudio excluído com sucesso!" });
            }
            catch (Exception ex)
            {
                return Json(new { sucesso = false, mensagem = "Erro ao excluir o áudio: " + ex.Message });
            }
        }
    }
}
