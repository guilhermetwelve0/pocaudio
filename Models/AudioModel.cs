using System;

namespace PocAudio.Models
{
    public class AudioModel
    {
        public int Id { get; set; }
        public string? Nome { get; set; }
        public string? Caminho { get; set; }
        public DateTime DataGravacao { get; set; }
    }
}