﻿using Microsoft.AspNetCore.Mvc;
using PocAudio.Models;

namespace PocAudio.Data
{
    public interface IAudioRepository
    {
        IEnumerable<AudioModel> ObterTodos();
        AudioModel ObterPorId(int id);
        void Adicionar(AudioModel audio);
        void Atualizar(AudioModel audio);
        void Remover(int id);
    }
}
